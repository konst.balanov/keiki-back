## 0. TESTOVOE ZADANIE KEIKI (BACKEND)
This package demonstrates all my weakness in software development.


## 1. INSTALL DEPENDENCIES
```
npm i
```

## 2. DB MIGRATION
Fill `./config/config.json` `development` field with your credentials to your database.
RUN
```
cd models
node seqModels.js
```

## 3. RUN SERVER
Be sure `3000` port is free.
RUN
```
npm start
```