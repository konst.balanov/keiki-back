const { Sequelize, Model, DataTypes } = require('sequelize');
const { development } = require('../config/config.json')
const sequelize = new Sequelize(development.database, development.username, development.password, {
  host: development.host,
  dialect: "mysql",
  define: { charset: 'utf8', dialectOptions: { collate: 'utf8_general_ci' }, }
});


class Route extends Model { }
Route.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,

  },
  cityA: DataTypes.STRING,
  cityB: DataTypes.STRING,
  distance: DataTypes.INTEGER,
  expected_revenue: DataTypes.INTEGER,
  car_type_id: DataTypes.INTEGER,
  car_id: DataTypes.INTEGER,
  status_id: DataTypes.INTEGER,
  date_start: DataTypes.DATE,
  date_done: DataTypes.DATE,
}, { sequelize, tableName: 'route', timestamps: false });

class Car extends Model { }
Car.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  model: { type: DataTypes.STRING },
  registration_mark: { type: DataTypes.STRING, unique: 'car_reg_number_unique' },
  purchase_date: { type: DataTypes.DATE },
  car_type_id: {
    type: DataTypes.INTEGER
  },
  mileage: { type: DataTypes.INTEGER },
  status_id: { type: DataTypes.INTEGER }

}, { sequelize, tableName: 'car', timestamps: false });

Car.hasOne(Route, { onDelete: 'SET NULL', onUpdate: 'RESTRICT', foreignKey: 'car_id' })

sequelize.sync({ force: true, alter: true }).then(_ => {
  Car.create({
    model: 'Toyota Priora',
    registration_mark: 'NN2929PP',
    purchase_date: '2017-12-20 00:00',
    car_type_id: 0,
    mileage: 10000
  });
  Car.create({
    model: 'Lanos Tanos',
    registration_mark: 'BOOMBOOM',
    purchase_date: '2008-02-10 00:00',
    car_type_id: 0,
    mileage: 150
  });
  Car.create({
    model: 'Vedro',
    registration_mark: 'why why',
    purchase_date: '1988-09-27 00:00',
    car_type_id: 0,
    mileage: 150
  });
  Car.create({
    model: 'KAMAZ',
    registration_mark: 'Pudge228',
    purchase_date: '2000-04-17 00:00',
    car_type_id: 1,
    mileage: 225000
  });
  Car.create({
    model: 'Belaz 3%',
    registration_mark: 'Vezu_Kartoshku',
    purchase_date: '1999-01-28 00:00',
    car_type_id: 1,
    mileage: 3
  });

  Route.create({
    cityA: 'Kyiv',
    cityB: 'Mexico',
    distance: 5000,
    expected_revenue: 500,
    car_type_id: 1,
    car_id: 1,
    date_start: '2020-01-02'
  });
  Route.create({
    cityA: 'Chernobyl',
    cityB: 'Moscow',
    distance: 2000,
    expected_revenue: 1,
    car_type_id: 0,
    car_id: 2,
    date_start: '2020-06-18'
  });
  Route.create({
    cityA: 'Radiant',
    cityB: 'Dire',
    distance: 2020,
    expected_revenue: 40000000,
    car_type_id: 1,
    date_start: '2020-08-24'
  });
  Route.create({
    cityA: 'Washington',
    cityB: 'Beatch',
    distance: 777,
    expected_revenue: 888,
    car_type_id: 1,
    date_start: '2020-10-02'
  });

})
