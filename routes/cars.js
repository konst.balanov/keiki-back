const express = require('express');
const router = express.Router();
const mysql = require('./mysql.js')

function validateCarBody(req, res, next) {
  if (
    req?.body?.model?.length < 30 &&
    req?.body?.registration_mark?.length < 30 &&
    req?.body?.purchase_date &&
    [0, 1].includes(req?.body?.car_type_id) &&
    req?.body?.mileage > 0
  ) {
    return next();
  } else {
    res.status(400).send('Invalid one or many fields')
  }
}

router.get('/count', function (req, res, next) {
  mysql.query('SELECT COUNT(id) count FROM car;')
    .then(data => res.json(data[0]))
    .catch(err => res.status(503).send(err))
})

router.get('/', function (req, res, next) {
  mysql.query('SELECT *, DATE_FORMAT(purchase_date, "%Y-%m-%D") as purchase_date FROM car')
    .then(data => res.json(data))
    .catch(err => res.status(503).send('error'))
});

router.get('/type/:type_id', function (req, res, next) {
  let type_id = req.params.type_id;
  mysql.query('SELECT car.* FROM car LEFT OUTER JOIN route ON route.car_id = car.id WHERE car.car_type_id = ? AND route.id IS NULL', [type_id])
    .then(data => res.json(data))
    .catch(err => res.status(503).send('error'))
});

router.get('/:id', function (req, res, next) {
  mysql.query({ sql: 'SELECT * FROM car LEFT OUTER JOIN route ON route.car_id = car.id WHERE car.id = ?', nestTables: true }, [req.params.id])
    .then(data => {
      res.json(data[0])
    })
    .catch(err => { res.status(503).send('Something went wrong') })
});

router.patch('/:id', validateCarBody, function (req, res, next) {
  mysql.query(`UPDATE car SET model = ?, registration_mark = ?, purchase_date = ?, car_type_id = ?, mileage = ? WHERE id = ?`,
    [req.body.model, req.body.registration_mark, req.body.purchase_date, req.body.car_type_id, req.body.mileage, req.params.id])
    .then(data => res.json(data))
    .catch(err => res.status(503).send('error'))
})

router.post('/', validateCarBody, function (req, res, next) {
  console.log(req.body)
  mysql.query(`INSERT INTO car (model, registration_mark, purchase_date, car_type_id, mileage) VALUES (?, ?, ?, ?, ?)`,
    [req.body.model, req.body.registration_mark, req.body.purchase_date, req.body.car_type_id, req.body.mileage])
    .then(data => res.json(data))
    .catch(err => { console.log(err); res.status(503).send('error') })
})

router.delete('/:id', function (req, res, next) {
  mysql.query('DELETE FROM car WHERE id = ?', [req.params.id])
    .then(data => res.json({ status: 'ok' }))
    .catch(err => res.status(503).send('error'))
})

module.exports = router;
