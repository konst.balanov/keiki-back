const express = require('express');
const router = express.Router();
const mysql = require('./mysql.js')

function validateRouteBody(req, res, next) {
  if (
    req?.body?.cityA?.length < 128 &&
    req?.body?.cityB?.length < 128 &&
    req?.body?.distance > 0 && req?.body?.distance < 40075 && // )
    [0, 1].includes(req?.body?.car_type_id) &&
    req?.body?.expected_revenue > 0 &&
    req?.body?.date_start
  ) {
    return next();
  } else {
    res.status(400).send('Invalid one or many fields')
  }
}

router.param('id', function (req, res, next) {
  if (req.params.id > 0) {
    next();
  } else {
    return res.status(501).send('Id required');
  }
})

router.get('/count', function (req, res, next) {
  mysql.query(`SELECT COUNT(id) count FROM route;`)
    .then(data => res.json(data[0]))
    .catch(err => res.status(503).send(err))
})

router.patch('/:id/assigncar/:car_id', function (req, res, next) {
  let id = req.params.id;
  let car_id = req.params.car_id;
  mysql.query('UPDATE route SET car_id = ? WHERE id = ?', [car_id, id])
    .then(data => res.json(data))
    .catch(err => res.status(503).send('err'))
})

router.get('/', function (req, res, next) {
  let offset = Number(req.query.page) * Number(req.query.pageSize);
  let count = Number(req.query.pageSize);

  mysql.query('SELECT route.*, car.registration_mark FROM route LEFT OUTER JOIN car ON route.car_id = car.id LIMIT 0, ?;', [count])
    .then(data => res.json(data))
    .catch(err => res.status(503).send('err'))
    .catch(console.log)
});

router.get('/:id', function (req, res, next) {
  mysql.query({ sql: 'SELECT * FROM route LEFT OUTER JOIN car ON route.car_id = car.id WHERE route.id = ?', nestTables: true }, [req.params.id])
    .then(data => {
      res.json(data[0])
    })
    .catch(err => { console.log('CATCH?', err); res.status(503).send('Something went wrong') })
});

router.post('/', validateRouteBody, function (req, res, next) {
  console.log(req.body)
  mysql.query(`INSERT INTO route (cityA, cityB, distance, expected_revenue, car_type_id, date_start) VALUES (?, ?, ?, ?, ?, ?)`,
    [req.body.cityA, req.body.cityB, req.body.distance, req.body.expected_revenue, req.body.car_type_id, req.body.date_start])

    .then(data => res.json(data))
    .catch(err => { console.log(err); res.status(503).send('error') })
})

router.put('/:id', validateRouteBody, function (req, res, next) {
  mysql.query(`UPDATE route SET cityA  = ?, cityB = ?, distance = ?, expected_revenue = ?, car_type_id = ?, date_start = ? WHERE id = ?`,
    [req.body.cityA, req.body.cityB, req.body.distance, req.body.expected_revenue, req.body.car_type_id, req.body.date_start, req.params.id])
    .then(data => res.json(data))
    .catch(err => { console.log(err); res.status(503).send('error') })
})

router.delete('/:id', function (req, res, next) {
  mysql.query('DELETE FROM route WHERE id = ?', [req.params.id])
    .then(data => res.json({ status: 'ok' }))
    .catch(err => res.status(503).send('error'))
})

module.exports = router;
