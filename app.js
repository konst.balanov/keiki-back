var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var routesRouter = require('./routes/routes');
var carsRouter = require('./routes/cars');

var app = express();


app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, PATCH");
	res.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');
	next()
})

app.use(express.urlencoded({ extended: false }))

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/routes', routesRouter);
app.use('/cars', carsRouter);

app.use('*', function(req, res) {
	res.sendFile(path.resolve(__dirname, 'public/index.html'))
})

module.exports = app;
